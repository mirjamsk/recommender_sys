# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 11:15:45 2015

@author: Mirjam
"""

from sklearn.cross_validation import train_test_split

from util_io import *
from UP import UserProfile
from IF import ItemFeatureMatrix
from URM import UserRatingMatrix
from US import UserSimilarityMatrix
from algorithms import ImplementedAlgorithms
from recommender import Recommender
from evaluator import Evaluator
 
data_location = r'./data/'

testUsers = read_test_users_csv(data_location + 'test.csv')
itemFeatures = read_item_features_csv(data_location + 'icm.csv')
userItemRating = read_user_items_csv(data_location  + 'train.csv')

max_itemId = userItemRating.itemId.max()
max_userId = userItemRating.userId.max()
max_featureId = itemFeatures.featureId.max()
trainUserItemRating, evalUserItemRating = train_test_split(userItemRating, test_size = 0.0)

userRatingMatrix =  UserRatingMatrix(
    userItemRating=trainUserItemRating, 
    max_itemId=max_itemId, 
    max_userId=max_userId)
    
itemFeatureMatrix = ItemFeatureMatrix(
    itemFeature=itemFeatures,
    max_itemId=max_itemId,
    max_featureId=max_featureId)

userProfile = UserProfile(
    userRatingMatrix=userRatingMatrix,
    itemFeatureMatrix=itemFeatureMatrix)
    

#userSimilarityMatrix = UserSimilarityMatrix(URM=userRatingMatrix.URM)

evaluator = Evaluator(
    true_user_item_ratings=evalUserItemRating,
    URM=userRatingMatrix,
    USM=None,
    IFM=itemFeatureMatrix)

#evaluator.set_algorithm(ImplementedAlgorithms.RIDGE_REGRESSION)
#evaluator.predict_data()
#evaluator.get_absolute_square_error()

recommender = Recommender(
    test_users=testUsers.values.T[0],
    URM=userRatingMatrix,
    USM=None,
    IFM=itemFeatureMatrix,
    UPM=userProfile)

recommender.set_algorithm(ImplementedAlgorithms.CONTENT_BASED)
predicted_recommendations = recommender.get_recommendations()

write_out_submission(
    predicted_recommendations=predicted_recommendations,
    file_path = data_location + 'submissions/dev_submission_rating_gt_7.csv')
