# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 14:16:03 2015

@author: mirjamskarica
"""
from algorithms import RecommendationAlgorithm


class Recommender(RecommendationAlgorithm):
    def __init__(self, test_users=None, URM=None, USM=None, IFM=None, UPM=None):
        RecommendationAlgorithm.__init__(self, URM=URM, USM=USM, IFM=IFM, UPM=UPM)
        self.test_users = test_users
        
    def get_recommendations(self):
        print('Init recommender: ' + self.predict_ratings_alg.__name__)
        predicted_recommendations = {}
        for user in self.test_users:
            print(user, end=', ' )
            predicted_ratings = self.predict_ratings_alg(user)
            predicted_recommendations[user] = self.users_top_N_recommendations(user, predicted_ratings)  
        print('End recommender: ' + self.predict_ratings_alg.__name__)
        return predicted_recommendations
    
    def users_top_N_recommendations(self, user=None, predicted_ratings=None, N=5):
        rated_movies = self.userRatingMatrix.URM[user].indices
        recommended_movies = predicted_ratings.argsort()[::-1]
        top_recommendations = []
        for i in range(recommended_movies.size):
            if len(top_recommendations) == 5: break
            if recommended_movies[i] in rated_movies: continue
            top_recommendations.append(recommended_movies[i])
        return top_recommendations
