# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 13:32:42 2015

@author: mirjamskarica
"""
from scipy.sparse import lil_matrix

# construct User Rating Matrix.URM
# --------------------------------
#           item1      item2
# user1   rating11    rating12
# user2   rating21    rating22
# --------------------------------

class ItemFeatureMatrix:
    def __init__(self, itemFeature=None, max_featureId=None, max_itemId=None):
        self.number_of_items = max_itemId + 1
        self.number_of_features = max_featureId + 1
        self.IF = self.construct_IF_from_panda_dataframe(itemFeature)
        
    def construct_IF_from_panda_dataframe(self,itemFeature):
        IF = lil_matrix((self.number_of_items, self.number_of_features), dtype=int)
               
        for index, itemId, featureId in itemFeature.itertuples():
            IF[itemId, featureId ] = 1
        return IF.tocsr()
        
    def has_user_rated_item(self, user, item):
        return True if self.binary_URM[user,item] else False

    def get_number_of_features(self):
        return self.number_of_features
        
    def get_number_of_items(self):
        return self.number_of_items
        