# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 14:12:35 2015

@author: mirjamskarica
"""
import numpy as np
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity

class UserSimilarityMatrix:
    def __init__(self, URM=None, metric='cosine'):
        self.US = None
        if metric == 'cosine':
            self.construct_cosine_similarity_matrix(URM)
        else:
            raise ValueError('No such metric: ' + metric)
        
    def construct_cosine_similarity_matrix(self, URM):
        self.US = cosine_similarity(URM)
        np.fill_diagonal(self.US, 0.0)
        self.US = sparse.csr_matrix(self.US)