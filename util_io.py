# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 21:04:13 2015

@author: Mirjam
"""
import pandas as pd


# returns: userItemRating DataFrame with the following sturcture:
# --------------------
# userId itemId rating
# 22     4567    5
# 323    13367  75
# --------------------
def read_user_items_csv(file_path):
    userItemRating = pd.read_csv(file_path)
    userItemRating[['userId','itemId']] = userItemRating[['userId','itemId']] - 1
    return userItemRating


# returns: testUsers DataFrame with the following sturcture:
# --------------------
# userId
# 22
# 323
# --------------------
def read_test_users_csv(file_path):
    testUsers = pd.read_csv(file_path)
    testUsers.userId = testUsers.userId - 1
    return testUsers


# returns: itemFeatures DataFrame with the following sturcture:
# --------------------
# itemId featureId
# 22      1
# 323     3
# --------------------
def read_item_features_csv(file_path):
    itemFeatures = pd.read_csv(file_path)
    itemFeatures[['itemId','featureId']] = itemFeatures[['itemId','featureId']] - 1
    return itemFeatures
 
# in: predicted_recommendations = {user_id: [i1, i2, i3, i4, i5]}   
def write_out_submission(predicted_recommendations, file_path):
    recommendations = pd.DataFrame.from_dict(predicted_recommendations, orient='index')
    recommendations = recommendations.sort_index()
    recommendations.index = recommendations.index + 1
    recommendations = recommendations + 1
    recommendations = recommendations.astype(str)
    recommendations = recommendations.iloc[:].apply(lambda x: ' '.join(x), axis=1)
    recommendations.to_csv(file_path, index_label='userId', header=['testItems'])
    print('Predicted recommendations written to ' + file_path)
    
    
class ConstantDict(object):
    """An enumeration class."""
    _dict = None

    @classmethod
    def dict(cls):
        """Dictionary of all upper-case constants."""
        if cls._dict is None:
            val = lambda x: getattr(cls, x)
            cls._dict = dict(((c, val(c)) for c in dir(cls) if c == c.upper()))
        return cls._dict

    def __contains__(self, value):
        return value in self.dict().values()

    def __iter__(self):
        for value in self.dict().values():
            yield value