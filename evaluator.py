# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 19:32:38 2015

@author: mirjamskarica
"""
from sklearn.metrics import mean_squared_error, mean_absolute_error
from algorithms import RecommendationAlgorithm


class Evaluator(RecommendationAlgorithm):
    
     def __init__(self, true_user_item_ratings=None, URM=None, USM=None, IFM=None):
        RecommendationAlgorithm.__init__(self, URM=URM, USM=USM, IFM=IFM)
        self.eval_users = true_user_item_ratings.userId.unique()
        self.true_user_item_ratings = true_user_item_ratings.copy(deep=True)
        self.true_user_item_ratings['rating'] = self.true_user_item_ratings['rating'].astype(float)
        self.predicted_user_item_ratings = self.true_user_item_ratings.copy(deep=True)
        
     def get_mean_squared_error(self):
        return mean_squared_error(
            y_true = self.true_user_item_ratings.sort().rating, 
            y_pred = self.predicted_user_item_ratings.sort().rating )**0.5
            
     def get_absolute_square_error(self):
        return mean_absolute_error(
            y_true = self.true_user_item_ratings.sort().rating, 
            y_pred = self.predicted_user_item_ratings.sort().rating )
            
     def predict_data(self):
        print('Init evaluator: ' + self.predict_ratings_alg.__name__)
        for user in self.eval_users:
            print(user, end=', ' )
            predicted_ratings =  self.predict_ratings_alg(user)
            self.set_predicted_ratings_for_user(user, predicted_ratings)
        print('End evaluatr: ' + self.predict_ratings_alg.__name__)
 
     def set_predicted_ratings_for_user(self, user, predicted_ratings):
         def filter_true_user_entries(user):
            return self.true_user_item_ratings[self.true_user_item_ratings.userId==user]
            
         for i in filter_true_user_entries(user).index:
            itemId = self.predicted_user_item_ratings.xs(i).itemId
            self.predicted_user_item_ratings.set_value(i, 'rating', predicted_ratings[itemId])
            
     
    