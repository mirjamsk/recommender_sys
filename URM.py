# -*- coding: utf-8 -*-
"""
Created on Sun Nov  1 13:32:42 2015

@author: mirjamskarica
"""
from scipy.sparse import lil_matrix

# construct User Rating Matrix.URM
# --------------------------------
#           item1      item2
# user1   rating11    rating12
# user2   rating21    rating22
# --------------------------------

class UserRatingMatrix:
    def __init__(self, userItemRating=None, max_userId=None, max_itemId=None):
        self.number_of_users = max_userId + 1
        self.number_of_items = max_itemId + 1
        self.URM, self.binary_URM, self.likes_URM = self.construct_URM_from_panda_dataframe(userItemRating)
        
    def construct_URM_from_panda_dataframe(self, UserItemRating):
        URM = lil_matrix((self.number_of_users, self.number_of_items), dtype=int)
        binary_URM = lil_matrix((self.number_of_users, self.number_of_items), dtype=int)
        likes_URM = lil_matrix((self.number_of_users, self.number_of_items), dtype=int)
        
        for index, userId, itemId, rating in UserItemRating.itertuples():
            URM[userId, itemId] = rating
            likes_URM[userId, itemId] = 1 if rating >= 6 else -1
            binary_URM[userId, itemId] = 1
        return URM.tocsr(), binary_URM.tocsr(), likes_URM.tocsr()

    def has_user_rated_item(self, user, item):
        return True if self.binary_URM[user,item] else False

    def get_number_of_users(self):
        return self.number_of_users
        
    def get_number_of_items(self):
        return self.number_of_items
        