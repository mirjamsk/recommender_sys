# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 02:18:47 2015

@author: Mirjam
"""

from scipy.sparse import dia_matrix
from sklearn import linear_model
import numpy as np

from util_io import ConstantDict

class ImplementedAlgorithms(ConstantDict):
    CONTENT_BASED     = 'content_based'
    RIDGE_REGRESSION  = 'ridge_regression'
    CONTENT_AND_RIDGE = 'content_and_ridge'
    USER_COLABORATIVE = 'user_collaborative'

    
class RecommendationAlgorithm:
    ImplementedAlgorithms = ImplementedAlgorithms()
    
    def __init__(self, URM=None, USM=None, IFM=None, UPM=None):
       
        self.predict_ratings_alg  = None
        self.userRatingMatrix     = URM
        self.itemFeatureMatrix    = IFM
        self.userProfileMatrix    = UPM
        self.userSimilarityMatrix = USM
        self.averaged_ratings     = self.get_averaged_ratings()

    
    def set_algorithm(self, alg_name=''):
        if alg_name not in self.ImplementedAlgorithms:
            print(alg_name + ' is not implemented')
        
        elif alg_name == self.ImplementedAlgorithms.RIDGE_REGRESSION:
            self.predict_ratings_alg = self.predict_ratings_using_ridge_regression
        
        elif alg_name == self.ImplementedAlgorithms.USER_COLABORATIVE:
            user_nb =  self.userRatingMatrix.get_number_of_users()
            self.similarity_coeffs = dia_matrix((user_nb , user_nb))
            self.predict_ratings_alg = self.predict_ratings_using_user_collaborative_filtering
        
        elif alg_name == self.ImplementedAlgorithms.CONTENT_BASED:
            self.predicted_URM = self.userProfileMatrix.UP.dot(self.itemFeatureMatrix.IF.transpose())
            self.predict_ratings_alg = self.predict_ratings_using_content_filtering
       
        elif alg_name == self.ImplementedAlgorithms.CONTENT_AND_RIDGE:
            self.predicted_URM = self.userProfileMatrix.UP.dot(self.itemFeatureMatrix.IF.transpose())
            self.predict_ratings_alg = self.predict_ratings_using_content_and_ridge_regression
            
    
    def get_averaged_ratings(self):
        denominator  = self.userRatingMatrix.binary_URM.sum(axis=0)
        denominator = np.array(denominator)[0]
        F = np.vectorize(lambda x: 1.0/x if x>4 else 0, 'f')
        
        summed_ratings = self.userRatingMatrix.URM.sum(axis=0)
        summed_ratings = np.array(summed_ratings)[0]
        averaged_ratings= summed_ratings * F(denominator)
        F = np.vectorize(lambda x: 5 if x<0.8 else x, 'f')
        return F(averaged_ratings)
        
        
    def predict_ratings_using_content_filtering(self, user):
        predicted_ratings = np.array(np.nan_to_num(self.predicted_URM[user].todense()))[0]
        return predicted_ratings
        
        
    def predict_ratings_using_user_collaborative_filtering(self, user):
        self.similarity_coeffs.setdiag(self.userSimilarityMatrix.US[user].toarray()[0])
        weighted_URM = self.similarity_coeffs.dot(self.userRatingMatrix.URM)
        denominator = self.similarity_coeffs.dot(self.userRatingMatrix.binary_URM).sum(axis=0)
        predicted_ratings = (weighted_URM.sum(axis=0)/denominator)
        predicted_ratings = np.array(np.nan_to_num(predicted_ratings))[0]
        return predicted_ratings
        
        
    def predict_ratings_using_ridge_regression(self, user):
        users_ratings = self.userRatingMatrix.URM[user]
        if users_ratings.size ==0: return self.averaged_ratings
        rated_items = users_ratings.indices
        ratings = users_ratings.data
        X = self.itemFeatureMatrix.IF[rated_items]
        ridge = linear_model.Ridge (alpha = .4)
        ridge.fit(X, ratings)
        predicted_ratings=ridge.predict(self.itemFeatureMatrix.IF)
        return predicted_ratings
        
        
    def predict_ratings_using_content_and_ridge_regression(self, user):
        users_ratings = self.userRatingMatrix.URM[user]
        if users_ratings.size < 11:
            return self.predict_ratings_using_content_filtering( user)
        rated_items = users_ratings.indices
        ratings = users_ratings.data
        X = self.itemFeatureMatrix.IF[rated_items]
        ridge =linear_model.Ridge (alpha = .5)
        ridge.fit(X, ratings)
        predicted_ratings=ridge.predict(self.itemFeatureMatrix.IF)
        return predicted_ratings